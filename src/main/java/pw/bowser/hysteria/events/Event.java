package pw.bowser.hysteria.events;

/**
 * Event container
 * Date: 1/16/14
 * Time: 3:17 PM
 */
public abstract class Event {

    private boolean cancelled = false;

    public final boolean isCancelled() { return cancelled; }

    public void cancel(){
        cancelled = true;
    }

    public abstract String getName();

}
