package pw.bowser.hysteria.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;

/**
 * Plugs in to an {@link pw.bowser.hysteria.events.EventHub} and receives events
 * @param <E>
 */
public interface Client<E extends Event> {

    /**
     * Handle incoming event
     * @param event
     */
    public void processEvent(E event) throws EventException;

    /**
     * Get the client priority
     *
     * @return priority
     */
    public int getClientPriority();

    /**
     * Allows for a method to be called as an event client
     *
     * @param <MT> type of event to receive
     */
    public static final class MethodClient<MT extends Event> implements Client<MT>{

        /**
         * Factory method to create a MethodClient.
         * @throws java.lang.IllegalArgumentException when a parameter arity mismatch, container type mismatch or an unreachable method error is found
         * @param m                 method that will be called as the event receiver
         * @param classInstance     an instance of the containing class
         * @param eventType         the type of the containing class
         * @param clientPriority    priority of the event client
         * @return                  new MethodClient
         */
        public static <T extends Event> MethodClient<T> fromMethod(Method m, Object classInstance, Class<T> eventType, int clientPriority){

            // Start by checking that the method has one parameter, an event of type <MT>
            if(!(m.getParameterTypes().length == 1 && m.getParameterTypes()[0].isAssignableFrom(eventType)))
                throw new IllegalArgumentException("Method must be of specified type");

            // Check that the method is accessible
            m.setAccessible(true);
            if(!m.isAccessible()) throw new IllegalArgumentException("Specified method is unreachable");

            // Construct a new one
            return new MethodClient<>(m, classInstance, clientPriority);

        }

        //--------------------------------------------------------------------------------------------------------------

        private final Method callTo;
        private final Object methodOwner;
        private final int priority;

        private MethodClient(Method aim, Object methodOwner, int priority){
            this.callTo         = aim;
            this.methodOwner    = methodOwner;
            this.priority       = priority;
        }


        @Override
        public void processEvent(MT event) throws EventException{

            if(event == null) throw new EventException("Null event passed to method client", null);

            try {
                callTo.invoke(methodOwner, event);
            } catch (InvocationTargetException e) {
                throw new EventException("Unable to invoke MethodClient against target", e, event);
            } catch (IllegalAccessException e) {
                throw new EventException("MethodClient ref method is unreachable from the method client.", e, event);
            }

        }


        @Override
        public int getClientPriority() {
            return priority;
        }
    }

    public static final class ClientComparator implements Comparator<Client<?>> {

        @Override
        public int compare(Client<?> o1, Client<?> o2) {
            return o2.getClientPriority() - o1.getClientPriority();
        }

    }

}
