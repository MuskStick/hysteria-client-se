package pw.bowser.hysteria.minecraft;

/**
 * Date: 1/23/14
 * Time: 9:59 AM
 */
public interface RenderJob {

    public void render();

}
