package pw.bowser.hysteria.minecraft

/**
 * OTF TickReceiver creation
 *
 * Date: 2/9/14
 * Time: 12:37 AM
 */
object Ticker {

  def apply(action: => Any): TickReceiver = new TickReceiver {
    def onTick(): Unit = action
  }

}
