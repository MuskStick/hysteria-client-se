package pw.bowser.hysteria.gui.overlay

import pw.bowser.hysteria.config.ConfigurationFlag

/**
 * Date: 2/13/14
 * Time: 11:35 AM
 */
object TextHUDPosition extends Enumeration {

  type TextHUDPosition = Value

  val TOP_LEFT      = Value("top.left")
  val TOP_RIGHT     = Value("top.right")
  val BOTTOM_LEFT   = Value("bottom.left")
  val BOTTOM_RIGHT  = Value("bottom.right")

  val TRANSFORM = ConfigurationFlag.Transforms.StringLike("top.left", "top.right", "bottom.left", "bottom.right")

}
