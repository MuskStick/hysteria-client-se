package pw.bowser.hysteria.engine

import pw.bowser.util.VersionData

/**
 * Get the version data from a manifest
 * Date: 2/21/14
 * Time: 5:56 PM
 */
case class StaticVersionData(version: String, vcsRevision: String,
                             vcsRevisionTime: String, buildTime: String) extends VersionData
