package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.entity.Entity
import pw.bowser.hysteria.events.PersistentEvent

/**
 * Event that is published when an entity disappears from the loaded world
 * Date: 2/13/14
 * Time: 9:27 PM
 */
class EntityDisappearEvent(entity: Entity) extends PersistentEvent {
  def getName: String = "EntityDisappear"
}
