package pw.bowser.hysteria.eventsimpl.world

import pw.bowser.hysteria.events.Event

/**
 * Broadcast before a block is damaged
 * Date: 2/16/14
 * Time: 12:46 PM
 */
class PlayerDamageBlockEvent(val blockX: Int,
                             val blockY: Int,
                             val blockZ: Int,
                             val side: Int) extends Event {
  def getName: String = "BlockDamage"
}
