package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.world.World
import pw.bowser.hysteria.events.{Event, PersistentEvent}

/**
 * Event broadcast before a world/server is entered
 *
 * Date: 2/13/14
 * Time: 10:27 PM
 */
class WorldPreEnterEvent(var hostname: String = "localhost",
                         var port: Int = 25565) extends Event {

  def getName: String = "WorldPreEnter"

}
