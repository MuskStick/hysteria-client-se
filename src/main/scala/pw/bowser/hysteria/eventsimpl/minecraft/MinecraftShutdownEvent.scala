package pw.bowser.hysteria.eventsimpl.minecraft

import pw.bowser.hysteria.events.Event

/**
 * Date: 2/18/14
 * Time: 8:56 AM
 */
class MinecraftShutdownEvent extends Event {
  def getName: String = "MinecraftShutdown"
}
