package pw.bowser.hysteria.commands.companion

import pw.hysteria.input.dashfoo.command.{Flag => FlagSpec}
import java.util
import pw.bowser.hysteria.engine.HysteriaBroker
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider
import scala.collection.convert.wrapAsJava.asJavaCollection
import pw.bowser.hysteria.commands.HysteriaCommand

/**
 * Create commands on the fly
 * Date: 2/5/14
 * Time: 9:26 PM
 */
object Command {

  /**
   * Create a flags-only command OTF
   * @param names command handles
   * @param flags command flags
   * @return command
   */
  def apply(names: Array[String], flags: FlagSpec*): HysteriaCommand = {
    Command(names, {(cargs:Array[String], flags:util.Map[String, Array[String]]) =>}, flags:_*)
  }

  /**
   * Create a flags-only command OTF
   * @param names command handles
   * @param help  command help
   * @param flags command flags
   * @return      command
   */
  def apply(names: Array[String], help: String ,flags: FlagSpec*): HysteriaCommand = {
    Command(names, {(cargs:Array[String], flags:util.Map[String, Array[String]]) =>}, help, flags:_*)
  }

  /**
   * Create a flags-only command OTF
   * @param names       command handles
   * @param help        command help
   * @param description command description
   * @param flags       command flags
   * @return            command
   */
  def apply(names: Array[String], help: String, description: String,flags: FlagSpec*): HysteriaCommand = {
    Command(names, {(cargs:Array[String], flags:util.Map[String, Array[String]]) =>}, help, description, flags:_*)
  }

  /**
   * Create a command OTF
   * @param names   command handles
   * @param action  command action
   * @return        command
   */
  def apply(names: Array[String], action: (Array[String], util.Map[String, Array[String]]) => Unit): HysteriaCommand = {
    Command(names, action, Array[FlagSpec]():_*)
  }

  /**
   * Create a command OTF
   * @param names   command handles
   * @param action  command action
   * @param flags   command flags
   * @return        command
   */
  def apply(names: Array[String], action: (Array[String], util.Map[String, Array[String]]) => Unit, flags: FlagSpec*): HysteriaCommand = {
    Command(names, action, null.asInstanceOf[String], flags:_*)
  }

  /**
   * Create a command OTF
   * @param names   command handles
   * @param action  command action
   * @param help    command help
   * @param flags   command flags
   * @return        command
   */
  def apply(names: Array[String], action: (Array[String], util.Map[String, Array[String]]) => Unit,
            help: String, flags: FlagSpec*): HysteriaCommand = {
    Command(names, action, help, null.asInstanceOf[String], flags:_*)
  }

  /**
   * Create a command OTF
   * @param names       handles
   * @param action      command action
   * @param help        command help
   * @param description command description
   * @param flags       command flags
   * @return            command
   */
  def apply(names: Array[String], action: (Array[String], util.Map[String, Array[String]]) => Unit,
            help: String, description: String, flags: FlagSpec*): HysteriaCommand = {

    new HysteriaCommand(names, HysteriaBroker.hysteria.injector.getInstance(classOf[CommandRegexProvider])) {

      flagger.addAll(asJavaCollection(flags))

      def getDescription: String = description

      def getHelp: String = help

      /**
       * Deferred command invocation
       *
       * @param commandArgs default flag arguments
       * @param flags       flags and their arguments
       */
      override def onCommand(commandArgs: Array[String], flags: util.Map[String, Array[String]]): Unit = action(commandArgs, flags)
    }

  }
}
