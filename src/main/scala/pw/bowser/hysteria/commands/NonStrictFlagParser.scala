package pw.bowser.hysteria.commands

import pw.hysteria.input.dashfoo.command.parsing.{CommandRegexProvider, UserInputFlagParser}
import scala.collection.JavaConversions._
import java.util

/**
 * This flag parser does not work in regard to flag arity settings. It will parse all things following
 *  each flag, mapping any number of arguments.
 *
 * Date: 2/6/14
 * Time: 10:35 AM
 */
class NonStrictFlagParser(val regexProvider: CommandRegexProvider) extends UserInputFlagParser {

  def mapUserInput(toParse: String): util.Map[String, Array[String]] = {
    /*
     * Working map for results
     */
    val working = new util.HashMap[String, Array[String]]()

    var flagWorking = "@default"
    var argsWorking = new util.ArrayList[String]
    /*
     * Exploder will split everything based on the pattern for exploding things
     */
    val exploder = regexProvider.getExplosionPattern.matcher(toParse)

    /*
     * This will be true once we find the command handle (this will be the first thing to happen, if we are using handles)
     */
    var caughtHandle = false

    /*
     * Iterate through each separated entity
     */
    while(exploder.find()){
      val elementCurrent  = regexProvider.postProcessString(exploder.group())

      /*
       * Throw out the handle
       */
      if(!caughtHandle && regexProvider.isCommand(elementCurrent))
        caughtHandle = true
      else if(regexProvider.isFlag(elementCurrent)) {
        /*
         * Register current flag in to map
         */
        working.put(flagWorking, argsWorking.toArray(Array[String]()))

        /*
         * Reassign working variables
         */
        flagWorking = regexProvider.getFlag(elementCurrent)
        argsWorking = new util.ArrayList[String]()

      } else
        // Always prefer the first group if it is available. this allows providers to add escapes and blockquotes, etc.
        argsWorking.add(if(exploder.group(1) != null) exploder.group(1) else elementCurrent)
    }

    /*
     * Add the last processed entry
     */
    working.put(flagWorking, argsWorking.toArray(Array[String]()))

    working

  }

}
