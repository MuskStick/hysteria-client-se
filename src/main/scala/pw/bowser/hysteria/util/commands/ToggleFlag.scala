package pw.bowser.hysteria.util.commands

import pw.bowser.hysteria.engine.HysteriaServices
import pw.hysteria.input.dashfoo.command.{HelpProvider, Flag}
import pw.bowser.hysteria.commands.SimpleHelpProvider
import pw.bowser.libhysteria.toggles.{ToggleService, ToggleState, Toggles}

/**
 * Lets us create a DashFoo flag otf for toggling a Toggleable
 * Date: 2/10/14
 * Time: 3:08 PM
 */
object ToggleFlag extends HysteriaServices {

  private var displaysNotificationClause: () => Boolean = {()=> true}

  def setNotifyClause(clause: () => Boolean) = displaysNotificationClause = clause
  def notifyClause: () => Boolean            = displaysNotificationClause

  /**
   * Create a flag to toggle a toggles
   * @param names   flag handles
   * @param toggles toggles
   * @return        flag
   */
  def apply(names: Array[String], toggles: Toggles)(implicit service: ToggleService): Flag = new Flag {

    val simpleHelp: HelpProvider =
      new SimpleHelpProvider(null, s"Toggles ${toggles.displayName}")

    def invokeFlag(args: String*): Unit = service.toggle(toggles) match {
      case ToggleState.Enabled =>
        if(notifyClause()) tellPlayer(s"Enabled toggle '${toggles.displayName}'")
      case ToggleState.Disabled =>
        if(notifyClause()) tellPlayer(s"Disabled toggle '${toggles.displayName}'")
    }

    def getArity: Int = 0

    def getHandles: Array[String] = names

    def getHelpProvider: HelpProvider = simpleHelp

  }

}
