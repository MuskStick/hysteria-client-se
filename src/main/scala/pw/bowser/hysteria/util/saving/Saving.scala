package pw.bowser.hysteria.util.saving

/**
 * Something that saves itself
 *
 * Date: 2/1/14
 * Time: 7:21 PM
 */
trait Saving {
  
  def save()

  def load()

}
