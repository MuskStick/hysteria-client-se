package pw.bowser.hysteria.util.saving

import pw.bowser.libhysteria.storage.saving.SavableSystem

/**
 * Central collection of `Saving`s that may be saved/loaded en mass
 *
 * Date: 2/1/14
 * Time: 7:22 PM
 */
object GlobalStorage extends SavableSystem
