package pw.bowser.hysteria.spm

import pw.bowser.spm.{VersionExpression, PluginManifest}

/**
 * PluginManifest that may be stored as a runtime-retained annotation
 */
case class SPMManifest(groupId: String, name: String, version: String,
                       mainClass: String = null,
                       dependencies: List[(String, String, VersionExpression)] = List())
                       extends scala.annotation.StaticAnnotation with PluginManifest
