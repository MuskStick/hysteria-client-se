package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 3/21/14
 * Time: 9:32 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Step",
  version     = "0.1",
  commands    = Array("st"),
  description = "Step up blocks"
)
class Step extends HysteriaModule with TogglesMixin with TickReceiver {


  var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()
    configuration.setPropertyIfNew("step.height",  1.25F)
    command = Command(Array("st", "step"), ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("s", "h", "size", "height"), configuration, "step.height", ConfigurationFlag.Transforms.FLOAT))
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // TickReceiver Implementation
  //--------------------------------------------------------------------------------------------------------------------

  def onTick(): Unit = if(minecraft.thePlayer != null) {
    minecraft.thePlayer.stepHeight = configuration.getFloat("step.height")
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Step"

  override def statusText: Option[String] = Some(configuration.getFloat("step.height").toString)

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
    if(minecraft.thePlayer != null) minecraft.thePlayer.stepHeight = 0.5F
  }

  override def distinguishedName: String = "pw.hysteria.step"
}
