package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import net.minecraft.client.settings.KeyBinding
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag

/**
 * Causes player to swim when submersed
 *
 * Date: 5/7/14
 * Time: 10:42 AM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "AutoSwim",
  version     = "0.1",
  description = "Enables jump when in water",
  commands    = Array("swim")
)
class AutoSwim extends HysteriaModule with TogglesMixin with TickReceiver {

  val command = Command(Array("swim"), ToggleFlag(Array("@if_none"), this))

  override def enableModule(): Unit = {
    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  override def onTick(): Unit = if(minecraftToolkit.isInWorld) KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindJump.getKeyCode, minecraftToolkit.thePlayer.isInWater)

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.auto_swim"

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "Swim"

  override def conflictingToggles: List[String] = List("pw.hysteria.zencantsnipe.water_walk")

  /**
   * Called when the toggles is enabled or required
   * The implementation should store logic to start and enable functionality here.
   */
  override def enable(): Unit = {
    tickService.register(this)
  }

  /**
   * Called when the toggles is disabled or suppressed.
   * The implementation should store logic to halt and disable functionality here.
   */
  override def disable(): Unit = {
    KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindJump.getKeyCode, false)
    tickService.remove(this)
  }

}
