package pw.bowser.hysteria.inject

import com.google.inject.AbstractModule

import pw.bowser.libhysteria.config.Configuration
import pw.bowser.hysteria.logging.LoggingToolkit

import java.io.File

import pw.bowser.libhysteria.storage.marshal.registrar.{FileStorageService, StorageService}
import pw.bowser.hysteria.events.EventHub

import pw.hysteria.input.dashfoo.command.CommandDispatcher
import pw.bowser.hysteria.chat.{NoHandleRegexProvider, CommandFormatFromConfig}
import pw.hysteria.input.dashfoo.command.parsing.{CommandRegexProvider, CommandFormattingProvider}
import pw.bowser.libhysteria.toggles.ToggleService
import pw.bowser.minecraft.players.PlayerDataManager
import pw.bowser.minecraft.servers.{ServerDataManager, FileBoundServerDataManager}
import pw.bowser.libhysteria.config.map.MapConfiguration
import pw.bowser.hysteria.util.saving.GlobalStorage

/**
 * Module containing engine and global dependencies
 * Date: 1/29/14
 * Time: 1:07 PM
 */
class HysteriaEngineBundle(val storageBase: File) extends AbstractModule {

  //--------------------------------------------------------------------------------------------------------------------

  /**
   * All engine-related bits are stored here
   */
  val hysteriaStorage: File = new File(storageBase, "engine")
  hysteriaStorage.mkdirs()

  /**
   * Engine-wide StorageMediaRegistrar
   */
  val systemStorageService: StorageService = new FileStorageService(hysteriaStorage)
  GlobalStorage.enroll(systemStorageService)

  /**
   * This is the application base config
   * It stores all of hysteria's settings
   */
  val systemConfiguration: Configuration = new MapConfiguration(handle = "hysteria")
  systemStorageService.enroll(systemConfiguration)

  //--------------------------------------------------------------------------------------------------------------------

  val logsStorage: File = new File(storageBase, "logs")
  logsStorage.mkdirs()

  val engineLoggingToolkit = new LoggingToolkit(logsStorage)

  //--------------------------------------------------------------------------------------------------------------------

  val commandFormat: CommandFormatFromConfig =
    new CommandFormatFromConfig(systemConfiguration, "commands.form.flag", "commands.form.escape", "commands.form.ignore", "commands.form.explode")

  val regexpProvider = new NoHandleRegexProvider(systemConfiguration, "commands.form.flag", "commands.form.escape", "commands.form.ignore")

  val commands: CommandDispatcher = new CommandDispatcher(regexpProvider)

  //--------------------------------------------------------------------------------------------------------------------

  val toggleableManagerService: ToggleService = new ToggleService
  systemStorageService.enroll(toggleableManagerService)

  //--------------------------------------------------------------------------------------------------------------------

  val eventHub: EventHub = new EventHub()

  //--------------------------------------------------------------------------------------------------------------------

  val sdmFolder = new File(hysteriaStorage, "servers")
  sdmFolder.mkdirs()

  val serverDataManager = new FileBoundServerDataManager(sdmFolder)

  //--------------------------------------------------------------------------------------------------------------------

  val pdmFolder = new File(hysteriaStorage, "players")
  pdmFolder.mkdirs()
  val pdmStorage = new FileStorageService(pdmFolder)
  val playerDataManager: PlayerDataManager = new PlayerDataManager(pdmStorage)
  GlobalStorage.enroll(pdmStorage)

  //--------------------------------------------------------------------------------------------------------------------

  def configure(): Unit = {

    /*
     * Configs and storage
     */

    bind(classOf[Configuration]) annotatedWith classOf[Engine] toInstance systemConfiguration

    bind(classOf[StorageService]) annotatedWith classOf[Engine] toInstance systemStorageService

    /*
     * Log toolkit
     */

    bind(classOf[LoggingToolkit]) toInstance engineLoggingToolkit

    /*
     * Events
     */

    bind(classOf[EventHub]) toInstance eventHub

    /*
     * Commands
     */

    bind(classOf[CommandDispatcher]) toInstance commands
    bind(classOf[CommandFormattingProvider]) toInstance commandFormat
    bind(classOf[CommandRegexProvider]) toInstance regexpProvider

    /*
     * Toggleables
     */

    bind(classOf[ToggleService]) toInstance toggleableManagerService

    /*
     * The overlay (is a managed singleton now, so there's no need to bind it)
     */

    /*
     * Player Data
     */

    bind(classOf[PlayerDataManager]) toInstance playerDataManager

    /*
     * Server Data
     */

    bind(classOf[ServerDataManager]) toInstance serverDataManager

  }
}