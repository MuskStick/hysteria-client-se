package pw.bowser.boot

import pw.bowser.hysteria.engine.HysteriaBroker
import net.minecraft.client.main.Main

/**
 * Date: 2/21/14
 * Time: 11:14 PM
 */
object StandardBootloader {

  def main(args: Array[String]) = {
    HysteriaBroker.setProvider(new StandardHysteriaProvider)
    Main.main(args)
  }

}
