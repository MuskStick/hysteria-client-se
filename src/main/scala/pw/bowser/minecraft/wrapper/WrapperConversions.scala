package pw.bowser.minecraft.wrapper

import _root_.net.minecraft.client.Minecraft
import _root_.net.minecraft.client.entity.EntityPlayerSP
import _root_.net.minecraft.client.gui.{GuiNewChat, GuiIngame, FontRenderer}
import _root_.net.minecraft.client.multiplayer.WorldClient
import _root_.net.minecraft.client.renderer.Tessellator
import _root_.net.minecraft.client.settings.GameSettings
import _root_.net.minecraft.entity.Entity
import _root_.net.minecraft.entity.player.EntityPlayer
import _root_.net.minecraft.world.{WorldProvider, World}
import pw.bowser.minecraft.wrapper.net.minecraft.client.HMinecraft
import pw.bowser.minecraft.wrapper.net.minecraft.client.gui.{HGuiNewChat, HGuiIngame, HFontRenderer}
import pw.bowser.minecraft.wrapper.net.minecraft.client.multiplayer.HWorldClient
import pw.bowser.minecraft.wrapper.net.minecraft.client.settings.HGameSettings
import pw.bowser.minecraft.wrapper.net.minecraft.entity.HEntity
import pw.bowser.minecraft.wrapper.net.minecraft.entity.player.{HEntityPlayerSP, HEntityPlayer}
import pw.bowser.minecraft.wrapper.net.minecraft.network.play.client.HClientPacketConversions
import pw.bowser.minecraft.wrapper.net.minecraft.network.play.server.HServerPacketConversions
import pw.bowser.minecraft.wrapper.net.minecraft.util.{HMovingObjectPosition, HEnumFacing, HVector3SupportConversions, HVector3}
import pw.bowser.minecraft.wrapper.net.minecraft.world.{HWorldProvider, HWorld}

/**
 * Converts MC types to what Hysteria is supposed to conform to
 */
trait WrapperConversions extends AnyRef with HClientPacketConversions with HServerPacketConversions
                                        with HVector3SupportConversions {

    implicit def hMinecraft4mcMinecraft(mc: Minecraft): HMinecraft = new HMinecraft(mc)

    // net.minecraft.util

    implicit def hv3formcv3(mcv: MCTypes.Vector3): HVector3 = new HVector3(mcv)
    implicit def hef4mcef(mcef: MCTypes.EnumFacing): HEnumFacing = new HEnumFacing(mcef)
    implicit def hmop4mcmop(mcmop: MCTypes.MovingObjectPosition): HMovingObjectPosition = new HMovingObjectPosition(mcmop)

    // net.minecraft.entity

    implicit def hEntity4mcEntity(mcEntity: Entity): HEntity = new HEntity(mcEntity)

    implicit def hEntityPlayer4mcEntityPlayer(mcEntityPlayer: EntityPlayer): HEntityPlayer = new HEntityPlayer(mcEntityPlayer)

    implicit def hEntityPlayerSP4mcEntityPlayerSP(mcEntityPlayerSP: EntityPlayerSP): HEntityPlayerSP = new HEntityPlayerSP(mcEntityPlayerSP)

    // net.minecraft.client.gui

    implicit def hFontRenderer4mcFontRenderer(mcFontRenderer: FontRenderer): HFontRenderer = new HFontRenderer(mcFontRenderer)

    implicit def hGuiIngame4mcGuiIngame(mcGuiIngame: GuiIngame): HGuiIngame = new HGuiIngame(mcGuiIngame)

    implicit def hGuiNewChat4mcGuiNewChat(mcGuiNewChat: GuiNewChat): HGuiNewChat = new HGuiNewChat(mcGuiNewChat)

    // net.minecraft.client.settings

    implicit def hGameSettings4mcGameSettings(mcGameSettings: GameSettings): HGameSettings = new HGameSettings(mcGameSettings)

    // net.minecraft.world

    implicit def hWorld4mcWorld(mcWorld: World): HWorld = new HWorld(mcWorld)
    implicit def hWProvider4mcWP(mcWP: WorldProvider): HWorldProvider = new HWorldProvider(mcWP)

    // net.minecraft.client.multiplayer

    implicit def hWorldClient4mcWorldClient(mcWorld: WorldClient): HWorldClient = new HWorldClient(mcWorld)

}

object WrapperConversions extends AnyRef with WrapperConversions
