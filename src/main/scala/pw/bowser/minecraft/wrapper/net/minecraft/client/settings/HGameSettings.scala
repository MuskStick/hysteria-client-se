package pw.bowser.minecraft.wrapper.net.minecraft.client.settings

import net.minecraft.client.settings.GameSettings

final class HGameSettings(val mcGameSettings: GameSettings) extends AnyVal {

    def useControlCodesInChat: Boolean = mcGameSettings.chatColours

}
