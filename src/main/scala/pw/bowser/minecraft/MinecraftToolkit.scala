package pw.bowser.minecraft

import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.network.Packet
import net.minecraft.client.gui.{GuiIngame, GuiScreen}
import pw.bowser.minecraft.entities.EntityToolkit
import pw.bowser.hysteria.minecraft.abstractionlayer.PlayerInventoryToolkit
import net.minecraft.item.Item
import net.minecraft.block.Block
import pw.bowser.minecraft.wrapper.MCTypes
import pw.bowser.minecraft.wrapper.net.minecraft.block.HBlock
import pw.bowser.minecraft.wrapper.net.minecraft.item.HItem

/**
 * Abstraction layer for dealing with the minecraft
 *
 * Date: 2/8/14
 * Time: 10:43 PM
 */
trait MinecraftToolkit {

  /*
   * Interacting with the player/as the player
   */

  /**
   * Add a line to chat
   *
   * @param what text
   */
  def tellPlayer(what: String)

  /**
   * Send a chat message
   *
   * @param what chat message
   */
  def sendChatMessage(what: String)

  /**
   * Get the player
   *
   * @return player
   */
  def thePlayer: EntityPlayerSP

  /*
   * Networking
   */

  /**
   * Send a packet to the server
   *
   * @param packet packet
   */
  def sendPacket(packet: MCTypes.Packet)

  /**
   * Connect to a server
   *
   * @param host host
   * @param port port
   */
  def connectToServer(host: String, port: Int)

  /**
   * Disconnect from the server currently connected to
   */
  def disconnectFromServer()

  /*
   * Engine
   */

  /**
   * Display a screen to the player
   *
   * @param screen screen instance
   */
  def displayGuiScreen(screen: GuiScreen)


  /**
   * Get the screen currently being displayed
   *
   * @return UI being displayed
   */
  def getCurrentGuiScreen: GuiScreen

  /**
   * Get ingame UI and components, etc...
   *
   * @return ingame UI
   */
  def getIngameUI: GuiIngame

  /*
   * World
   */

  /**
   * Whether or not the player is in a world
   *
   * @return is in world
   */
  def isInWorld: Boolean

  /**
   * Get the entity toolkit
   *
   * @return entity toolkit
   */
  def getEntityToolkit: EntityToolkit

  /**
   * Inform the game engine to re render the world
   */
  def markTerrainForUpdate(): Unit

  /*
   * Chat
   */

  /**
   * Split text in to lines that fit chat
   *
   * @param text  text
   * @return      split text
   */
  def makeChatLines(text: String): Array[String]

  /**
   * Get the player inventory toolkit instance
   *
   * @return player inventory toolkit
   */
  def inventoryToolkit: PlayerInventoryToolkit

}

object MinecraftToolkit {
  object StringTransforms {
    /** @deprecated use [[pw.bowser.minecraft.wrapper.net.minecraft.item.HItem HItem]] instead */
    val Item: String => Option[Item] = HItem.withNameOrId

    /** @deprecated use [[pw.bowser.minecraft.wrapper.net.minecraft.block.HBlock HBlock]] instead */
    val Block: String => Option[Block] = HBlock.withNameOrId
  }
}
