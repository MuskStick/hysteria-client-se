package pw.bowser.minecraft.servers

import scala.collection.{mutable => m}

import net.minecraft.client.multiplayer.{ServerData => MCServerData}
import java.io.File
import pw.bowser.libhysteria.storage.saving.Savable
import pw.bowser.libhysteria.storage.marshal.registrar.StorageService
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Contains data about a server
 * Date: 3/24/14
 * Time: 2:59 PM
 */
class ServerData (val mcData: MCServerData,
                  val storageFolder: File,
                  storage: StorageService) extends Savable {

  /**
   * Companions are models that store data about a server
   */
  private val companions  = m.Map[Class[_], Marshalling]()

  /**
   * Helpers are like companions, but are not stored.
   * They exist more to provide functionality than to store data
   */
  private val helpers     = m.Map[Class[_], AnyRef]()

  /**
   * Add a companion
   * @param companionClass companion class
   * @param companion companion instance
   * @tparam T companion type
   */
  def addCompanion[T <: Marshalling](companionClass: Class[T], companion: T): Unit = {
    if(companions.contains(companionClass)) throw new IllegalArgumentException("Companion already exists")
    companions.put(companionClass, companion)
    storage.enroll(companion)
  }

  /**
   * Get a companion instance
   * @param companionClass companion class
   * @tparam T companion type
   * @return companion
   */
  def getCompanion[T <: Marshalling](companionClass: Class[T]): T = {
    companions(companionClass).asInstanceOf[T]
  }

  /**
   * Remove a companion
   */
  def rmCompanion(companionClass: Class[_]): Unit = companions.remove(companionClass) match {
    case s: Some[Marshalling] => storage.disenroll(s.get)
    case None => // ignored
  }

  /**
   * Add a helper to the server data
   * @param helperClass helper class
   * @param helper helper instance
   * @tparam T helper type
   */
  def addHelper[T <: AnyRef](helperClass: Class[T], helper: T): Unit =
    if(helpers.contains(helperClass))
      throw new IllegalArgumentException("Helper already exists")
    else
      helpers.put(helperClass, helper)

  /**
   * Get a helper instance
   * @param helperClass helper class
   * @tparam T helper type
   * @return helper instance
   */
  def getHelper[T <: AnyRef](helperClass: Class[T]): T = helpers(helperClass).asInstanceOf[T]

  /**
   * Remove a helper instance
   * @param helperClass helper class
   */
  def rmHelper(helperClass: Class[_]): Unit = helpers.remove(helperClass)

  /**
   * Load server storage backend
   */
  def load(): Unit = storage.load()

  /**
   * Save server storage backend
   */
  def save(): Unit = storage.save()
}