package pw.bowser.testboot

import java.io.File

import pw.bowser.hysteria.engine.HysteriaBroker
import net.minecraft.client.main.Main

/**
 * Date: 2/21/14
 * Time: 10:46 PM
 */
object TestBootLoader {




  def main(args: Array[String]) = {

    val systemProperties = Map(
    )

    // Set up launch params.
    val mcDir = new File(System.getProperty("user.home"), ".minecraft")

    val defaultArgs = Map(
      "--username"       -> "Hysteria",
      "--accessToken"    -> "0",
      "--version"        -> "Hysteria-Testing",
      "--userProperties" -> "",
      "--assetIndex"     -> "1.8",
      "--gameDir"        -> mcDir.getAbsolutePath,
      "--assetsDir"      -> new File(mcDir, "assets").getAbsolutePath
    )

    println(defaultArgs)

    HysteriaBroker.setProvider(new TestHysteriaProvider)

    systemProperties.foreach({case(k, v) => System.setProperty(k, v)})

    var argsCompiled = args

    defaultArgs.foreach({case(param, value) =>
      if(!argsCompiled.contains(param)) {
        argsCompiled ++= (param :: value :: Nil)
      }
    })

    Main.main(argsCompiled)

  }

}
