package pw.bowser.testboot

import com.google.inject.AbstractModule
import pw.bowser.util.VersionData

/**
 * Date: 2/21/14
 * Time: 10:41 PM
 */
class TestingBundle extends AbstractModule {
  def configure(): Unit = {
    bind(classOf[VersionData]) toInstance TestingVersionData
  }
}
