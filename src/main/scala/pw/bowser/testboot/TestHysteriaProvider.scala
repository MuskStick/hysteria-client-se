package pw.bowser.testboot

import pw.bowser.hysteria.engine.{Hysteria, HysteriaProvider}
import net.minecraft.client.Minecraft
import scala.None
import com.google.inject.Guice
import pw.bowser.hysteria.inject.HysteriaEngineBundle
import java.io.File
import pw.bowser.hysteria.inject.minecraft.MinecraftBundle

/**
 * Handles Hysteria instance management for testing
 * Date: 2/21/14
 * Time: 10:48 PM
 */
class TestHysteriaProvider extends HysteriaProvider {

  private var theHysteria: Option[Hysteria] = None

  def initialize(mc: Minecraft) = {
    val hysteriaStorageBase = new File(mc.mcDataDir, "hysteria")

    val theInjector = Guice.createInjector(new TestingBundle,
                                           new HysteriaEngineBundle(hysteriaStorageBase),
                                           new MinecraftBundle(mc))

    theHysteria = Some(new Hysteria(hysteriaStorageBase, theInjector))
    theHysteria.get.configureHysteria()
  }

  /**
   * Get hysteria
   *
   * @return Hysteria
   */
  def hysteria: Hysteria = theHysteria match {
    case None               => throw UninitializedFieldError("Hysteria not yet initialized")
    case h: Some[Hysteria]  => h.get
  }

}
