package pw.bowser.util

import com.berico.similarity.JaroWinklerSimilarity

/**
 * Jaro-Winkler string-similarity function.
 *
 * Date: 2/18/14
 * Time: 5:44 PM
 */
object JaroWinkler {

  private val calculator = new JaroWinklerSimilarity

  def apply(subject: String, comparison: String): Double = calculator.calculate(subject, comparison) match {
    case Double.NaN => 0    // This fixes a NaN issue that can occur with short strings and case differences
    case d: Double  => d
  }

}
