package pw.bowser.util.mvel

import org.mvel2.templates.{TemplateCompiler, CompiledTemplate, TemplateRegistry}
import java.util
import scala.collection.mutable.{Map => MutableMap}
import scala.collection.JavaConversions.setAsJavaSet
import java.io.File

/**
 * Date: 2/25/14
 * Time: 10:25 AM
 */
class LazyFileTemplateRegistry(templateDir: File) extends TemplateRegistry {

  private val knownTemplates = MutableMap[String, CompiledTemplate]()

  def iterator(): util.Iterator[_ <: AnyRef] = getNames.iterator()

  def getNames: util.Set[String] = {
    println("getnames")

    // FP can be fun!
    setAsJavaSet(
      (templateDir.list().filter(_.matches(LazyFileTemplateRegistry.FILE_NAME_REGEX)).map(_.split(".mv")(0)) ++ knownTemplates.keySet).distinct.toSet)
  }

  def contains(name: String): Boolean = templateDir.list().contains(s"$name.mv") || knownTemplates.contains(name)

  def addNamedTemplate(name: String, template: CompiledTemplate): Unit = knownTemplates.put(name, template)

  def getNamedTemplate(name: String): CompiledTemplate = getOrLoadTemplate(name)

  /**
   * Get or load a template from the fs or map
   *
   * @param name  template name
   * @return      template
   */
  def getOrLoadTemplate(name: String): CompiledTemplate = knownTemplates.get(name) match {
    case tpl: Some[CompiledTemplate] => tpl.get
    case None =>
      val tplFile = new File(templateDir, LazyFileTemplateRegistry.FILE_NAME_FMT.format(name))
      if (tplFile.exists()) {
        addNamedTemplate(name, TemplateCompiler.compileTemplate(tplFile))
        getOrLoadTemplate(name)
      } else null
  }
}

object LazyFileTemplateRegistry {
  val FILE_NAME_FMT = "%s.mv"
  val FILE_NAME_REGEX = "(.*)\\.mv"
}
